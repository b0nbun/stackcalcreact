/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'

export const wrapper = css`
min-width: 100vw;
min-height: 100vh;
display: flex;
align-items: center;
justify-content: center;
`

export const calcbox = css`
position: relative;
display: flex;
flex-direction: row;
gap: 1rem;
background: #2f2f2f;
padding: 2rem;
border-radius: 1rem;
color: white;
`

export const innerbox = css`
font-size: 1rem;
padding: .5em; 
color: inherit;
border-radius: .3rem;
border: none;
`

export const textareabox = css`
resize: none;
background: #1f1f1f;
line-height: calc(1.5rem + .2rem);
overflow-y: hidden;
box-sizing: content-box;
width: 25ch;
vertical-align: top;
display: flex;
`

export const resultbox = css`
display: flex;
flex-direction: column;
gap: .2rem;
min-width: 9ch;

`

export const resultitem = css`
height: 1.5rem;
padding: .2em .5em;
display: flex;
align-items: center;
justify-content: center;
background-color: #1f1f1f;
border-radius: inherit;
transition: background-color .2s ease;

&:hover, &:focus {
    background-color: #242424;
    outline: 2px solid #0060DF;
}
`

export const hiddenresultitem = css`
background-color: transparent;
color: transparent;
`

export const errorstyle = (error : string) =>
    css`
        background-color: darkred;
        padding: .5ch;
        border-radius: inherit;

        &:hover, &:focus {
            background-color: crimson;
        }
    
        &::after {
            text-align: left;
            font-family: monospace;
            display: block;
            background-color: #3f3f3f;
            color: #fff;
            border-radius: .3rem;
            white-space: pre;
            pointer-events: none;
            content: "${error.replace(/\n/g, '\\a')}";
            width: fit-content;
            padding: .2em .5em;
            position: absolute;
            top: 0%;
            left: 50%;
            transform: translate(-50%, -50%);
            opacity: 0;
            transition:
                opacity .2s ease,
                transform .2s ease,
                top .2s ease,
                left .2s ease;
        }

        &:hover::after, &:focus::after {
            opacity: 1;
            top: 0%;
            left: 50%;
            transform: translate(-50%, calc(-100% - .8rem));
        }
    `