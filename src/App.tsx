/** @jsxImportSource @emotion/react */
import { calcbox, innerbox, resultbox, errorstyle, textareabox, wrapper, resultitem, hiddenresultitem } from './styles'
import React, { useEffect, useState, useRef } from 'react'
import evaluate, { Status, EErrorObject, repeat } from './evaluation'
import { css, Global } from '@emotion/react'

// TODO: Macros
// TODO: Help page/modal


export default function App() {
    const [input, setInput] = useState('')
    const [results, setResults] = useState([evaluate('')])
    const textareaRef : React.LegacyRef<HTMLTextAreaElement> = useRef(null)
    useEffect(() => {
        setResults([])
        if (textareaRef === null) return
        textareaRef.current!.style.height = "auto";
        textareaRef.current!.style.height = (textareaRef.current!.scrollHeight) + "px";
    }, [])
    
    const handleChange = (e : React.ChangeEvent<HTMLTextAreaElement>) => {
        requestAnimationFrame(() => {
            e.target.style.height = "auto";
            e.target.style.height = (e.target.scrollHeight) + "px";
        })
        const newInput = e.target.value
        setInput(newInput)
        setResults(newInput.split('\n').map(eq => evaluate(eq)))
    }

    const formatError = (error : EErrorObject) : string => {
        if (error.index === null) {
            return `${error.message}`
        }
        return `
${error.originalString}
${repeat(' ', error.index+1)}^ ${error.message}
stack: ${error.stack.join(', ')}
        `.trim()
    }

    const copyToClipboard = (s : string) => {
        console.log(s, 'copied')
        navigator.clipboard.writeText(s)
    }
    
    return (
        <>
        <Global styles={css`
            :root {
                font-size: 20px;
            }
        
            *, *::after, *::before {
                box-sizing: border-box;
            }
        
            body {
                font-family: sans-serif;
                position: relative;
                top: 0;
                left: 0;
                min-height: 100vh;
                width: 100vw;
                margin: 0;
                padding: 0;
            }
        `}/>
        <div css={wrapper}>
            <div css={calcbox}>
                <textarea ref={textareaRef} spellCheck={false} css={[innerbox, textareabox]}
                value={input} onChange={handleChange}/>
                <div css={[innerbox, resultbox]}>
                    {results.map((result, idx) =>
                        <div
                            tabIndex={0}
                            key={idx}
                            onClick={result.status === Status.Ok ? () => copyToClipboard(result.stack.join(' ')) : () => {}}
                            onKeyPress={result.status === Status.Ok ? ((e) => e.key === 'Enter' ? copyToClipboard(result.stack.join(' ')) : null) : () => {}}
                            css={[
                                resultitem,
                                result.status === Status.Error ? errorstyle(formatError(result.error)) : null,
                                result.status === Status.Ok && result.stack.length === 0 ? hiddenresultitem : null,
                            ]}
                        >{result.status === Status.Error ? 'Error' : result.stack.join(' ')}</div>
                    )}
                </div>
            </div>
        </div>
        </>
    )
}